﻿namespace WebApp.Models
{
    [TypeScriptModule("Models")]
    public class Foo
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
}