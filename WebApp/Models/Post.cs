﻿using System;

namespace WebApp.Models
{
    [TypeScriptModule("Models")]
    public class Post
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Type { get; set; }
        public PostTitle Title { get; set; }
        public string Content { get; set; }
    }

    [TypeScriptModule("Models")]
    public class PostTitle
    {
        public string Rendered { get; set; }
    }
}