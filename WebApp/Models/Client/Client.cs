﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Models.Client
{
    [TypeScriptModule("Client")]
    public class Client
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
    }
}