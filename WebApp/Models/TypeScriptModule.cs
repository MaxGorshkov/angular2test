﻿using System;

namespace WebApp.Models
{
    [System.AttributeUsage(AttributeTargets.Class)]
    sealed class TypeScriptModule : Attribute
    {
        private readonly string moduleName;
        public TypeScriptModule(string moduleName)
        {
            this.moduleName = moduleName;            
        }
    }
}