﻿declare module Models {
	interface Post {
		id: number;
		date: Date;
		type: string;
		title: Models.PostTitle;
		content: string;
	}
	interface PostTitle {
		rendered: string;
	}
}
