﻿/// <reference path="../../Models/Post.cs.d.ts"/>
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class PostService {
  constructor(private http: Http) { }

  public getAll(): Promise<Models.Post[]> {
    return this.http
      .get('http://idei-prazdnika.ru/wp-json/wp/v2/posts')
      .toPromise()
      .then((r: Response) => r.json() as Models.Post[]);
  }
}