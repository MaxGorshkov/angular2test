import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent, HomeComponent } from './app.component';
import { PostComponent } from './pageComponents/post.component';
import { AppRoutingModule } from './app.routing';
import { PostService } from './Services/postService';

@NgModule({
  imports: [BrowserModule
    , HttpModule
    , AppRoutingModule
  ],
  declarations: [AppComponent, PostComponent, HomeComponent],
  bootstrap: [AppComponent],
  providers: [PostService]
})
export class AppModule { }
