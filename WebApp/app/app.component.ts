/// <reference path="../Models/Foo.cs.d.ts"/>
/// <reference path="../Models/Client/Foo.cs.d.ts"/>
import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';

import { PostService } from './Services/PostService'

@Component({
  selector: 'my-app',
  providers: [PostService],
  templateUrl: '/app/app.html'
})
export class AppComponent {
  public posts: Models.Post[];  

  constructor(private postService: PostService) {
    this.postService.getAll().then(data => this.posts = data);    
  }
}

@Component({
  selector: 'home-app',  
  template: ''
})
export class HomeComponent { }
