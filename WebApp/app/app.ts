﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { PostComponent } from './PageComponents/post.component';

const routes: Routes = [
  { path: '', component: AppComponent },
  { path: 'post/:id', component: PostComponent }  
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }